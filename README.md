# Sample-Flask-Project

This is a simple Flask app was practice follow by [Flask Web Development](https://www.flaskbook.com/) book 

## Clone
- Clone this repo to your local machine using.

```
git clone https://gitlab.com/vuducmanh96vp/sample-flask-project.git
```

# Setting up project
## Create an environment
On Windows:
- Install virtualenv
```
pip install virtualenv
```
- Launch virtualenv
```
cd sample-flask-project
virtualenv venv
venv\Scripts\activate.bat
```

## Install requirements
```
pip install -r requirements.txt
```

## Create database
On Mysql:
> By default, Please create a database name is `test`, the username is `root`, the password is `null` for dev environment

*If you had a database and you want to connect the application with it then you can change the line `DEV_DATABASE_URL` in the `.env` file for dev environment follow below:*
```
DEV_DATABASE_URL = 'mysql+pymysql://username:password@localhost/db_name'
```

## Migrate database
[Flask-Migrate](https://flask-migrate.readthedocs.io/en/latest/)
- By default
```
set DEV_DATABASE_URL=mysql+pymysql://root:@localhost/test
python manage.py db upgrade
```

## Runserver
> By default command, it will be running  on `host` 127.0.0.1 `port` 5000
```
python manage.py runserver
```
*If If you want to run the server on other `host` and `port` then you run command by below:*
```
python manage.py runserver -h host -p port
```

## Run tests
```
set TEST_DATABASE_URL=mysql+pymysql://root:@localhost/test_db
python manage.py test
```