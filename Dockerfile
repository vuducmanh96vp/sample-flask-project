FROM python:3.6-alpine
COPY . /app
WORKDIR /app
ENV FLASK_ENV develop
ENV FLASK_DEBUG true
ENV FLASK_CONFIG dev
ENV DEV_DATABASE_URL mysql+pymysql://root:@localhost/test
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
CMD ["python", "manage.py", "runserver", "-h","0.0.0.0"]
