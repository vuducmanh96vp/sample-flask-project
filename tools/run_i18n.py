import argparse
import os

base_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../'))
config_path = os.path.join(base_path, 'config')

EXTRACT_COMMAND = 'pybabel extract -F babel.cfg  -k lazy_gettext -o messages.pot ../app'
UPDATE_COMMAND = 'pybabel update -i messages.pot -d ../app/translations'
COMPILE_COMMAND = 'pybabel compile -d ../app/translations'


def update_command():
    os.system('cd ' + config_path + ' && ' + EXTRACT_COMMAND)
    os.system('cd ' + config_path + ' && ' + UPDATE_COMMAND)


def compile_command():
    os.system('cd ' + config_path + ' && ' + COMPILE_COMMAND)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--update", help="update translations")
    parser.add_argument("--compile", help="Compile")
    args = parser.parse_args()
    if args.update:
        update_command()
    elif args.compile:
        compile_command()
    else:
        print('Nothing execute. Choosen --update or --compile')
