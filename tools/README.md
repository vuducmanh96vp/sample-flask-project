
# Translation
## Translation commands
>  Extracting texts for translation
```
cd config
pybabel extract -F babel.cfg -o messages.pot ../app
```

>  Generating a language catalog
```
pybabel init -i messages.pot -d ../app/translations -l vi
```

>   Updating the translations
```
pybabel extract -F babel.cfg  -k lazy_gettext -o messages.pot ../app
pybabel update -i messages.pot -d ../app/translations
```

>   Compile
```
cd config
pybabel compile -d ../app/translations
```

## Translation by run_i18n
> Updating the translations
```
cd tools
python run_i18n --update 1
```
> Compile
```
python run_i18n --compile
```