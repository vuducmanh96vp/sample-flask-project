import os

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Shell

from app import create_app, db, login_manager
from app.main.models import User

app = create_app(os.getenv('FLASK_CONFIG') or 'default')

manager = Manager(app)

# Handles SQLAlchemy database migrations
# https://flask-migrate.readthedocs.io/en/latest/
migrate = Migrate(app, db)


@manager.command
def test():
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)


def make_shell_context():
    return dict(app=app, db=db, User=User)


manager.add_command('shell', Shell(make_context=make_shell_context()))
manager.add_command('db', MigrateCommand)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


if __name__ == '__main__':
    # TODO load_dotenv()
    manager.run()
