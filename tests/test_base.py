import os
import unittest
from unittest import TestCase

from flask import current_app

from app import create_app, db


class BasesTestCase(TestCase):
    def setUp(self):
        # TODO Check os.getenv
        self.app = create_app(os.getenv('FLASK_CONFIG_TEST') or 'test')
        self.app_content = self.app.app_context()
        self.app_content.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_content.pop()

    def test_app_exist(self):
        self.assertFalse(current_app is None)

    def test_app_is_testing(self):
        self.assertTrue(current_app.config['TESTING'])


if __name__ == "__main__":
    unittest.main()
