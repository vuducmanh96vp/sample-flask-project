# -*- coding: utf-8 -*-

from .dev import DevConfig
from .product import ProductionConfig
from .test import TestingConfig

config = {
    'dev': DevConfig,
    'test': TestingConfig,
    'product': ProductionConfig,

    'default': DevConfig
}
LANGUAGES = {
    'en': 'en',
    'vi': 'vi'
}
