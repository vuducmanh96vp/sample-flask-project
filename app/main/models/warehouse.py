from app import db
from app.main.models.base import BaseModel


class Warehouse(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    description = db.Column(db.String(32), nullable=False)
    quantity = db.Column(db.Integer)

    def __repr__(self):
        return '<Warehouse %r>' % self.description
