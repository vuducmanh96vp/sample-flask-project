from app import db
from app.main.models.base import BaseModel


class Product(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    category_id = db.Column(
        db.Integer, db.ForeignKey('category.id'), nullable=False
    )
    name = db.Column(db.String(24))
    price = db.Column(db.Float)
    barcode = db.Column(db.String(32))
    image_url = db.Column(db.Text)
    warehouses = db.relationship('Warehouse', backref='products', lazy=True)

    def __repr__(self):
        return '<Product %r>' % self.name
