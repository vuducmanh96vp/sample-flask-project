from datetime import datetime

from app import db


class BaseModel(db.Model):
    # SQLAlchemy not to create the table for a particular model.
    # https://stackoverflow.com/a/22977281
    __abstract__ = True
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, default=datetime.now())
