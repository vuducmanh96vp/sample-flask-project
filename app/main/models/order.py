from app import db
from app.main.models.base import BaseModel


class Order(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    ticket_id = db.Column(
        db.Integer, db.ForeignKey('ticket.id'), nullable=False
    )
    item = db.Column(db.String(12))
    quantity = db.Column(db.Integer)
    total_amount = db.Column(db.Integer)

    def __repr__(self):
        return '<Order %r>' % self.item
