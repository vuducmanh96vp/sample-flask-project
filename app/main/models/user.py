from flask_login import UserMixin

from .base import BaseModel
from app import db


class User(UserMixin, BaseModel):
    # override the table name
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(36), nullable=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password = db.Column(db.Text)
    token = db.Column(db.Text)
    phone_number = db.Column(db.String(13), nullable=True)
    email = db.Column(db.String(120), unique=True, nullable=True)
    is_active = db.Column(db.Boolean, default=True)

    def __repr__(self):
        return '<User %r>' % self.username

    @classmethod
    def check_user_exists(cls, username):
        user = User.query.filter_by(username=username).all()
        if user:
            return True
        return False
