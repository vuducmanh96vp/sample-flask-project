from app import db
from app.main.models.base import BaseModel


class Ticket(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(
        db.Integer, db.ForeignKey('customer.id'), nullable=True
    )
    total_amount = db.Column(db.Float)
    quantity = db.Column(db.Integer)

    def __repr__(self):
        return '<Ticket %r>' % self.id
