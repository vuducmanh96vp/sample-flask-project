import enum

from app import db
from app.main.models.base import BaseModel


class Gender(enum.Enum):
    MALE = "Male"
    FEMALE = "Female"
    OTHER = "Other"


class Customer(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(36))
    gender = db.Column(db.Enum(Gender), default=1)
    phone_number = db.Column(db.String(12), nullable=True)
    birthday = db.Column(db.DateTime, nullable=True)
    address = db.Column(db.String(120), nullable=True)
    email = db.Column(db.String(36), nullable=True)
    tickets = db.relationship('Ticket', backref='customer', lazy=True)

    def __repr__(self):
        return '<Customer %r>' % self.full_name
