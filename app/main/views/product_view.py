from datetime import datetime

from flask import render_template, request, redirect, url_for, flash
from flask_login import login_required

from app import db
from app.main.contants import (
    CREATE_SUCCESS, UPDATE_SUCCESS, DELETE_SUCCESS,
    ID_NOT_FOUND
)
from app.main.forms import ProductForm
from app.main.models import Product
from app.main_blueprint import main


@main.route('/product', methods=['GET'])
@login_required
def list_product():
    return render_template(
        'product/list.html', products=Product.query.all()
    )


@main.route('/product/add', methods=['GET', 'POST'])
@login_required
def add_product():
    product_form = ProductForm()
    if request.method == 'POST' and product_form.validate_on_submit():
        data = dict()
        data['category_id'] = product_form.category_id.data.id
        data['barcode'] = product_form.barcode.data
        data['name'] = product_form.name.data
        data['price'] = product_form.price.data
        data['image_url'] = product_form.image.data
        product = Product(**data)
        db.session.add(product)
        db.session.commit()
        flash(CREATE_SUCCESS)
        return redirect(url_for('main.list_product'))

    return render_template('product/add.html', form=product_form)


@main.route('/product/edit/<int:product_id>', methods=['GET', 'POST'])
@login_required
def edit_product(product_id):
    product = Product.query.filter_by(id=product_id).first()
    if not product:
        flash(ID_NOT_FOUND)
        return redirect(url_for('main.list_product'))

    product_form = ProductForm()
    if product_form.validate_on_submit():
        product.name = request.form.get('name')
        product.barcode = request.form.get('barcode')
        product.category_id = request.form.get('category_id')
        product.price = request.form.get('price')
        product.image = request.form.get('image_url')
        product.updated_at = datetime.now()
        db.session.commit()
        flash(UPDATE_SUCCESS)
        return redirect(url_for('main.list_product'))
    product_form.name.data = product.name
    product_form.barcode.data = product.barcode
    if product.category:
        product_form.category_id.data = product.category
    product_form.price.data = int(product.price)
    # TODO add image
    product_form.image.data = product.image_url
    return render_template('product/edit.html', form=product_form)


@main.route('/product/delete/<int:product_id>', methods=['GET', 'POST'])
def delete_product(product_id):
    product = Product.query.filter_by(id=product_id)
    if product:
        product.delete()
        db.session.commit()
        flash(DELETE_SUCCESS)
    else:
        flash(ID_NOT_FOUND)
    return redirect(url_for('main.list_product'))
