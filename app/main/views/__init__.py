from .views import *
from .authenticate import *
from .category_view import *
from .customer_view import *
from .product_view import *
from .sale_view import *
from .warehouse_view import *
