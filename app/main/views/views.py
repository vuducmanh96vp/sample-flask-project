import uuid

from flask import render_template, request
from flask_login import login_required

from app.main_blueprint import main
from ..models import User
from app import db


@main.route('/', methods=['GET', 'POST'])
@login_required
def index():
    return render_template(
        'index.html',  users=User.query.all()
    )

