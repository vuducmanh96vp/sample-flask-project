from datetime import datetime

from flask import render_template, request, flash, redirect, url_for
from flask_login import login_required

from app import db
from app.main.contants import (
    CREATE_SUCCESS, DELETE_SUCCESS, UPDATE_SUCCESS,
    ID_NOT_FOUND
)
from app.main.models import Customer
from app.main.forms import CustomerForm
from app.main_blueprint import main


@main.route('/customer', methods=['GET'])
@login_required
def list_customer():
    return render_template(
        'customer/list.html', customers=Customer.query.all()
    )


@main.route('/customer/add', methods=['GET', 'POST'])
@login_required
def add_customer():
    customer_form = CustomerForm()
    if request.method == 'POST' and customer_form.validate_on_submit():
        customer = Customer()
        customer.full_name = request.form.get('full_name')
        customer.gender = request.form.get('gender')
        customer.phone_number = request.form.get('phone_number')
        customer.birthday = request.form.get('birthday')
        customer.address = request.form.get('address')
        customer.email = request.form.get('email')
        db.session.add(customer)
        db.session.commit()
        flash(CREATE_SUCCESS)
        return redirect(url_for('main.list_customer'))
    return render_template('customer/add.html', form=customer_form)


@main.route('/customer/edit/<int:customer_id>', methods=['GET', 'POST'])
@login_required
def edit_customer(customer_id):
    customer = Customer.query.filter_by(id=customer_id).first()
    if not customer:
        flash(ID_NOT_FOUND)
        return redirect(url_for('main.list_customer'))

    customer_form = CustomerForm()
    if request.method == 'POST' and customer_form.validate_on_submit():
        customer.full_name = request.form.get('full_name')
        customer.gender = request.form.get('gender')
        customer.phone_number = request.form.get('phone_number')
        customer.birthday = request.form.get('birthday')
        customer.address = request.form.get('address')
        customer.email = request.form.get('email')
        customer.updated_at = datetime.now()
        db.session.commit()
        flash(UPDATE_SUCCESS)
        return redirect(url_for('main.list_customer'))
    customer_form.full_name.data = customer.full_name
    customer_form.phone_number.data = customer.phone_number
    customer_form.birthday.data = customer.birthday
    customer_form.address.data = customer.address
    customer_form.email.data = customer.email
    return render_template('customer/edit.html', form=customer_form)


@main.route('/customer/delete/<int:customer_id>', methods=['GET', 'POST'])
@login_required
def delete_customer(customer_id):
    customer = Customer.query.filter_by(id=customer_id)
    if customer:
        customer.delete()
        db.session.commit()
        flash(DELETE_SUCCESS)
    else:
        flash(ID_NOT_FOUND)
    return redirect(url_for('main.list_customer'))
