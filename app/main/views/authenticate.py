import hashlib
import uuid

from flask import request, redirect, url_for, render_template, flash
from flask_login import login_user, login_required, logout_user

from app import db
from app.main_blueprint import main
from ..contants.messages import USER_EXISTS, REGISTER_SUCCESS
from ..models import User
from ..forms import LoginForm, RegisterForm, ForgetForm


@main.route('/login', methods=['POST', 'GET'])
def login():
    login_form = LoginForm()
    if request.method == 'POST' and login_form.validate_on_submit():
        username = request.form.get('username')
        password = hashlib.md5(
            request.form.get('password').encode('utf-8')
        ).hexdigest()
        remember = request.form.get('remember')
        user = User.query.filter_by(
            username=username, password=password
        ).first()
        if user:
            next_url = request.args.get('next')
            if remember:
                login_user(user, True)
            else:
                login_user(user)
            # TODO is_safe_url should check if the url is safe for redirects.
            #  See http://flask.pocoo.org/snippets/62/ for an example.
            return redirect(next_url or url_for('main.index'))
    return render_template('auth/login.html', form=login_form)


@main.route('/register', methods=['POST', 'GET'])
def register():
    register_form = RegisterForm()
    if request.method == 'POST' and register_form.validate_on_submit():
        username = request.form.get('username')
        if User.check_user_exists(username):
            return render_template(
                'auth/register.html', form=register_form, message=USER_EXISTS
            )
        user_data = dict()
        user_data['username'] = username
        user_data['uuid'] = str(uuid.uuid4())
        # TODO add salt
        user_data['password'] = hashlib.md5(
            request.form.get('password').encode('utf-8')
        ).hexdigest()
        user_data['phone_number'] = request.form.get('phone_number')
        user_data['email'] = request.form.get('email')
        user = User(**user_data)
        db.session.add(user)
        db.session.commit()
        flash(REGISTER_SUCCESS)
        return redirect(url_for('main.login'))
    return render_template('auth/register.html', form=register_form)


@main.route('/forget', methods=['GET', 'POST'])
def forget():
    forget_form = ForgetForm()
    # TODO process forget pass
    return render_template('auth/forget.html', form=forget_form)


@main.route('/logout', methods=['POST', 'GET'])
@login_required
def logout():
    logout_user()
    return redirect('login')
