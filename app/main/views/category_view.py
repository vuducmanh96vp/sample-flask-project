from datetime import datetime

from flask import request, render_template, flash, url_for, redirect
from flask_login import login_required

from app import db
from app.main.contants import (
    UPDATE_SUCCESS, DELETE_SUCCESS, CREATE_SUCCESS,
    ID_NOT_FOUND
)
from app.main.forms.category_form import CategoryForm
from app.main.models import Category
from app.main_blueprint import main


@main.route('/category/', methods=['GET'])
@login_required
def list_category():
    return render_template(
        'category/list.html', categories=Category.query.all()
    )


@main.route('/category/add', methods=['GET', 'POST'])
@login_required
def add_category():
    category_form = CategoryForm()
    if request.method == 'POST' and category_form.validate_on_submit():
        name = request.form.get('name')
        try:
            category = Category(name=name)
            db.session.add(category)
        except Exception:
            db.session.rollback()
            raise Exception
        db.session.commit()
        flash(CREATE_SUCCESS)
        if category_form.save_and_continue.data:
            return redirect(url_for('main.add_category'))
        return redirect(url_for('main.list_category'))

    return render_template('category/add.html', form=category_form)


@main.route('/category/edit/<int:category_id>', methods=['POST', 'GET'])
@login_required
def edit_category(category_id):
    category = Category.query.filter_by(id=category_id).first()
    if not category:
        flash(ID_NOT_FOUND)
        return redirect(url_for('main.list_category'))
    category_form = CategoryForm()
    if category_form.validate_on_submit():
        category.name = request.form.get('name')
        category.updated_at = datetime.now()
        db.session.commit()
        flash(UPDATE_SUCCESS)
        return redirect(url_for('main.list_category'))
    category_form.name.data = category.name
    return render_template(
        'category/edit.html', form=category_form, categories=category
    )


@main.route('/category/delete/<int:category_id>', methods=['POST', 'GET'])
@login_required
def delete_category(category_id):
    category = Category.query.filter_by(id=category_id)
    if category:
        category.delete()
        db.session.commit()
        flash(DELETE_SUCCESS)
    else:
        flash(ID_NOT_FOUND)
    return redirect(url_for('main.list_category'))
