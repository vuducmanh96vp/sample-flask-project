from flask_login import login_required

from app.main_blueprint import main


@main.route('/sale', methods=['GET'])
@login_required
def sale():
    return 'Sale'
