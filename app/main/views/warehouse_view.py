import datetime

from flask import render_template, request, redirect, url_for, flash
from flask_login import login_required

from app import db
from app.main.contants import (
    CREATE_SUCCESS, UPDATE_SUCCESS, DELETE_SUCCESS,
    ID_NOT_FOUND
)
from app.main.forms import WarehouseForm
from app.main.models import Warehouse
from app.main_blueprint import main


@main.route('/warehouse', methods=['GET'])
@login_required
def list_warehouse():
    return render_template(
        'warehouse/list.html', warehouses=Warehouse.query.all()
    )


@main.route('/warehouse/add', methods=['GET', 'POST'])
@login_required
def add_warehouse():
    warehouse_form = WarehouseForm()
    if request.method == 'POST' and warehouse_form.validate_on_submit():
        warehouse = Warehouse()
        warehouse.product_id = int(warehouse_form.product_id.data.id)
        warehouse.quantity = warehouse_form.quantity.data
        db.session.add(warehouse)
        db.session.commit()
        flash(CREATE_SUCCESS)
        return redirect(url_for('main.list_warehouse'))
    return render_template('warehouse/add.html', form=warehouse_form)


@main.route('/warehouse/edit/<int:warehouse_id>', methods=['GET', 'POST'])
@login_required
def edit_warehouse(warehouse_id):
    warehouse = Warehouse.query.filter_by(id=warehouse_id).first()
    if not warehouse:
        flash(ID_NOT_FOUND)
        return redirect(url_for('main.list_warehouse'))
    warehouse_form = WarehouseForm()
    if request.method == 'POST' and warehouse_form.validate_on_submit():
        warehouse.product_id = request.form.get('product_id')
        warehouse.quantity = request.form.get('quantity')
        warehouse.description = request.form.get('description')
        warehouse.updated_at = datetime.datetime.now()
        db.session.commit()
        flash(UPDATE_SUCCESS)
        return redirect(url_for('main.list_warehouse'))
    if warehouse.products:
        warehouse_form.product_id.data = warehouse.products
    warehouse_form.description.data = warehouse.description
    warehouse_form.quantity.data = warehouse.quantity
    return render_template('warehouse/edit.html', form=warehouse_form)


@main.route('/warehouse/delete/<int:warehouse_id>', methods=['GET'])
@login_required
def delete_warehouse(warehouse_id):
    warehouse = Warehouse.query.filter_by(id=warehouse_id)
    if warehouse:
        warehouse.delete()
        db.session.commit()
        flash(DELETE_SUCCESS)
    else:
        flash(ID_NOT_FOUND)
    return redirect(url_for('main.list_warehouse'))
