import locale

from app.main_blueprint import main


@main.app_template_filter()
def format_currency(value):
    locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')
    return locale.currency(value, symbol=True, grouping=True)


@main.app_template_filter()
def format_date(value):
    return value.strftime('%d/%m/%Y')
