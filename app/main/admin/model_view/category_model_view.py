from .base_model_view import BaseModelView


class CategoryModelView(BaseModelView):
    column_exclude_list = ['created_at', 'updated_at']
    column_searchable_list = ['name']
    column_filters = ['name']
    column_editable_list = ['name']
