from .base_model_view import BaseModelView


class ProductModelView(BaseModelView):
    column_exclude_list = ['created_at', 'updated_at']
    column_searchable_list = ['name', 'barcode']
    column_filters = ['name']
