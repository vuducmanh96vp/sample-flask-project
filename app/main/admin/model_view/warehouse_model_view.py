from .base_model_view import BaseModelView


class WarehouseModelView(BaseModelView):
    column_exclude_list = ['created_at', 'updated_at']
    column_searchable_list = ['product_id']
    column_filters = ['product_id']
