import flask_login
from flask import url_for, redirect, request
from flask_admin.contrib.sqla import ModelView


class BaseModelView(ModelView):
    create_modal = True
    edit_modal = True
    can_export = True
    can_view_details = True
    page_size = 20  # the number of entries to display on the list view
    # TODO check is_accessible
    # def is_accessible(self):
    #     return flask_login.current_user.is_authenticated
    #
    # def inaccessible_callback(self, name, **kwargs):
    #     # redirect to login page if user doesn't have access
    #     return redirect(url_for('main.login', next=request.url))
