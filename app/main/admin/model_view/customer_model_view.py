from .base_model_view import BaseModelView


class CustomerModelView(BaseModelView):
    can_view_details = True
    page_size = 10  # the number of entries to display on the list view
    column_exclude_list = ['created_at', 'updated_at']
    column_searchable_list = ['phone_number', 'full_name', 'email']
    column_filters = ['full_name']
