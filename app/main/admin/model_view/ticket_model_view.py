from .base_model_view import BaseModelView


class TicketModelView(BaseModelView):
    column_exclude_list = ['created_at', 'updated_at']
    column_searchable_list = ['customer_id']
    column_filters = ['customer_id']
