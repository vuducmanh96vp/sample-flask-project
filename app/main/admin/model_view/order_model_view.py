from .base_model_view import BaseModelView


class OrderModelView(BaseModelView):
    column_exclude_list = ['created_at', 'updated_at']
    column_searchable_list = ['item']
    column_filters = ['item']
