from .base_model_view import BaseModelView


class UserModelView(BaseModelView):
    column_searchable_list = ['username', 'phone_number', 'email']
    column_filters = ['username', 'phone_number', 'email']
    column_exclude_list = ['password', 'token']
