from app import db
from .model_view import (
    CategoryModelView, CustomerModelView, OrderModelView,
    UserModelView, ProductModelView, TicketModelView, WarehouseModelView
)
from app.main.models import (
    User, Category, Customer, Order,
    Ticket, Warehouse, Product
)


class AdminView:
    @staticmethod
    def register_view(admin):
        # TODO check category
        admin.add_views(UserModelView(User, db.session))
        admin.add_views(CategoryModelView(Category, db.session))
        admin.add_view(CustomerModelView(Customer, db.session))
        admin.add_view(OrderModelView(Order, db.session))
        admin.add_view(ProductModelView(Product, db.session))
        admin.add_view(TicketModelView(Ticket, db.session))
        admin.add_view(WarehouseModelView(Warehouse, db.session))
