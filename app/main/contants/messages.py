# Authenticate User
USER_EXISTS = 'Username exists!'
REGISTER_SUCCESS = 'Register success!'

# CRUD
CREATE_SUCCESS = 'Create Success!'
UPDATE_SUCCESS = 'Updated success!'
DELETE_SUCCESS = 'Delete success!'

# ERROR
ID_NOT_FOUND = 'ID not found'
