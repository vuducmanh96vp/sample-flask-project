from flask import jsonify

from app.main import Product
from app.main_blueprint import api


@api.route('/product', methods=['GET'])
def get_user():
    products = Product.query.all()
    product_list = []
    for product in products:
        product_dict = dict()
        product_dict['category'] = product.category.name
        product_dict['name'] = product.name
        product_dict['price'] = product.price
        product_dict['barcode'] = product.barcode
        product_dict['image_url'] = product.image_url
        product_list.append(product_dict)
    res = {
        'Status': '200',
        'Data': product_list
    }
    return jsonify(res)
