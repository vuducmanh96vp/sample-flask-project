from flask_babel import lazy_gettext
from flask_wtf import FlaskForm
from wtforms import (
    StringField, SubmitField, DateField,
    SelectField
)
from wtforms.fields.html5 import EmailField
from wtforms.validators import InputRequired, Length, Regexp, length, Email

from app.main.contants.regex_pattern import PATTERN_PHONE_NUMBER


class CustomerForm(FlaskForm):
    full_name = StringField(
        lazy_gettext('Full Name'),
        render_kw={'class': 'form-control'},
        validators=[InputRequired(), length(min=4)]
    )
    gender = SelectField(
        lazy_gettext('Gender'),
        render_kw={'class': 'form-control'},
        choices=[
            ('MALE', lazy_gettext('Male')),
            ('FEMALE', lazy_gettext('Female')),
            ('OTHER', lazy_gettext('Other'))
        ]
    )
    phone_number = StringField(
        lazy_gettext('Phone Number'),
        render_kw={'class': 'form-control'},
        validators=[Regexp(
            regex=PATTERN_PHONE_NUMBER,
            message='Invalid Phone number. Please try again!'
        )]
    )
    birthday = DateField(
        lazy_gettext('Birthday'),
        render_kw={'class': 'form-control'},
        validators=[InputRequired()]
    )
    address = StringField(
        'Address', render_kw={'class': 'form-control'},
        validators=[
            InputRequired(), Length(min=4)
        ]
    )
    email = EmailField(
        'Email', render_kw={'class': 'form-control'},
        validators=[
            InputRequired(), Email()
        ]
    )
    submit = SubmitField(
        lazy_gettext('Submit'),
        render_kw={'class': 'btn btn-primary'}
    )
