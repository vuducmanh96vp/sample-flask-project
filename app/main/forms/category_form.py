from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import InputRequired, Length


class CategoryForm(FlaskForm):
    name = StringField(
        'Category Name', render_kw={'class': 'form-control'},
        validators=[
            InputRequired(), Length(min=4)
        ]
    )
    save = SubmitField('Save', render_kw={'class': 'btn btn-primary'})
    save_and_continue = SubmitField(
        'Save & continue',
        render_kw={'class': 'btn btn-info'}
    )
