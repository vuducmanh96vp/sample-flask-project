from flask_babel import lazy_gettext
from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from wtforms import (
    StringField, IntegerField, FileField,
    SubmitField
)
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from app.main.models import Category


class ProductForm(FlaskForm):
    category_id = QuerySelectField(
        lazy_gettext('Category'),
        query_factory=lambda: Category.query.all(),
        get_pk=lambda category: category.id,
        get_label=lambda category: category.name,
        render_kw={"class": 'custom-select'},
    )
    name = StringField(
        lazy_gettext('Name'),
        render_kw={'class': 'form-control'}
    )
    price = IntegerField(
        lazy_gettext('Price'),
        render_kw={'class': 'form-control'}
    )
    barcode = StringField(
        lazy_gettext('Barcode'),
        render_kw={'class': 'form-control'}
    )
    image = FileField(
        lazy_gettext('Image'),
        render_kw={'class': 'form-control'},
        validators=[
            FileAllowed(['jpg', 'png'], 'Images only!')
        ])
    submit = SubmitField(
        lazy_gettext('Submit'),
        render_kw={'class': 'btn btn-primary'}
    )
