from flask_wtf import FlaskForm
from wtforms import IntegerField, SubmitField


class SaleForm(FlaskForm):
    customer_id = IntegerField('Customer')
    total_amount = IntegerField('Total Amount')
    quantity = IntegerField('Quantity')
    submit = SubmitField()
