from flask_babel import lazy_gettext
from flask_wtf import FlaskForm
from wtforms import IntegerField, StringField, SubmitField
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from app.main.models import Product


class WarehouseForm(FlaskForm):
    product_id = QuerySelectField(
        lazy_gettext('Product'),
        query_factory=lambda: Product.query.all(),
        get_pk=lambda product: product.id,
        get_label=lambda product: product.name,
        render_kw={'class': 'custom-select'},
    )
    description = StringField(
        lazy_gettext('Description'),
        render_kw={'class': 'form-control'},
        default='Nhập hàng'
    )
    quantity = IntegerField(
        lazy_gettext('Quantity'),
        render_kw={'class': 'form-control'}
    )
    submit = SubmitField(
        lazy_gettext('Submit'),
        render_kw={'class': 'btn btn-primary'}
    )
