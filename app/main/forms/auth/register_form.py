from flask_babel import lazy_gettext
from flask_wtf import FlaskForm
from wtforms import (
    StringField, validators, PasswordField,
    SubmitField
)
from wtforms.fields.html5 import EmailField

from app.main.contants.regex_pattern import PATTERN_PASS, PATTERN_PHONE_NUMBER


class RegisterForm(FlaskForm):
    username = StringField(
        lazy_gettext('Username'),
        render_kw={'class': 'form-control'},
        validators=[validators.InputRequired(), validators.length(min=4)]
    )
    password = PasswordField(
        lazy_gettext('Password'),
        render_kw={'class': 'form-control'},
        validators=[
            validators.InputRequired(),
            validators.Regexp(regex=PATTERN_PASS)
        ]
    )
    phone_number = StringField(
        lazy_gettext('Phone Number'),
        render_kw={'class': 'form-control'},
        validators=[validators.Regexp(
            regex=PATTERN_PHONE_NUMBER,
            message='Invalid Phone number. Please try again!'
        )]
    )
    email = EmailField(
        lazy_gettext('Email'),
        render_kw={'class': 'form-control'},
        validators=[validators.Email()]
    )
    submit = SubmitField(
        lazy_gettext('Register'),
        render_kw={'class': 'btn btn-primary'}
    )
