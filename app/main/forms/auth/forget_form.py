from flask_babel import lazy_gettext
from flask_wtf import FlaskForm
from wtforms import (
    validators, SubmitField
)
from wtforms.fields.html5 import EmailField


class ForgetForm(FlaskForm):
    email = EmailField(
        lazy_gettext('Email'),
        render_kw={'class': 'form-control'},
        validators=[validators.Email()]
    )
    submit = SubmitField(
        lazy_gettext('Register'),
        render_kw={'class': 'btn btn-primary'}
    )
