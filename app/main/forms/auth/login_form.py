from flask_babel import lazy_gettext
from flask_wtf import FlaskForm
from wtforms import (
    StringField, validators, PasswordField,
    SubmitField
)


class LoginForm(FlaskForm):
    username = StringField(
        lazy_gettext('Username'),
        render_kw={'class': 'form-control',
                   "placeholder": lazy_gettext('Sign in')},
        validators=[validators.InputRequired(), validators.Length(min=4)]
    )
    password = PasswordField(
        lazy_gettext('Password'),
        render_kw={'class': 'form-control'},
        validators=[validators.InputRequired(), validators.length(min=4)]
    )
    submit = SubmitField(
        lazy_gettext('Login'),
        render_kw={'class': 'btn btn-primary'}
    )
