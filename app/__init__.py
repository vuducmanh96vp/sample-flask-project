from flask import Flask, request, session
from flask_admin import Admin
from flask_babel import Babel
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

from config import config, LANGUAGES

db = SQLAlchemy()
login_manager = LoginManager()
# redirect to login URL when a user attempts to access a login_required
# view without being logged in
login_manager.login_view = 'main.login'


def create_app(config_name):
    app = Flask(__name__)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    # set optional bootswatch theme for Flask-Admin
    app.config['FLASK_ADMIN_SWATCH'] = 'Default'
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    # Initialize Flask-Babel
    babel = Babel(app, default_locale=LANGUAGES.get('en'))

    @babel.localeselector
    def get_locale():
        # TODO select lang in screen
        if request.args.get('lang'):
            session['lang'] = request.args.get('lang')
            return session.get('lang', 'en')
        if session.get('lang') and not request.args.get('lang'):
            return session.get('lang', 'en')
        # Use the browser's language preferences to select
        # an available translation
        translations = LANGUAGES.keys()
        return request.accept_languages.best_match(translations)

    login_manager.init_app(app)
    db.init_app(app)

    from .main_blueprint import main, api
    app.register_blueprint(main)
    app.register_blueprint(api)

    # initialize an empty admin interface for your Flask app
    admin = Admin(app, name='Flask-Admin', template_mode='bootstrap3')
    # Adding Model Views
    from .main.admin import AdminView
    AdminView.register_view(admin)

    return app
